// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Puntuaciones.h"
#include "DatosMemCompartida.h"

class MundoCliente  
{
public:
	void Init();
	MundoCliente();
	virtual ~MundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Puntuaciones punt;

	int fdb; //Descriptor de fichero para mmap
	int fdsc; //Descriptor de fichero para el FIFO que conecta el servidor con el cliente
	int fdt; //Descriptor de fichero para el FIFO que envia las teclas

	int puntos1;
	int puntos2;

	DatosMemCompartida datosMem; //atributo del tipo DatosMemCompartida
	DatosMemCompartida *pdatosMem; //puntero datosMemCOmpartida
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
