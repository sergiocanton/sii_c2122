#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "DatosMemCompartida.h"

int main(int argc, char *argv[]){
int fd;
DatosMemCompartida *pdatosMem;
fd = open("datosBot", O_RDWR); // se abre el fichero de proyeccion
if (fd<0){
perror("Error en apertura");
}
pdatosMem = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0));

while(1){
	if(pdatosMem -> esfera.centro.y < pdatosMem -> raqueta1.y2){
	pdatosMem -> accion = -1;
	}
	if(pdatosMem -> esfera.centro.y > pdatosMem -> raqueta1.y1){
	pdatosMem -> accion = 1;
        }
	if(pdatosMem -> esfera.centro.y == ((pdatosMem -> raqueta1.y1)-(pdatosMem -> raqueta1.y2)/2)){
        pdatosMem -> accion = 0;
	}
	usleep(25000);

}
unlink("datosBot");
return 0;


}
