#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "Puntuaciones.h"

int main(int argc, char **argv){
int fd;
Puntuaciones punt;

unlink("FIFO_logger"); //borra el FIFO por si existe previamente

//creamos el FIFO
if (mkfifo("FIFO_logger",0666)<0){
perror("No puede crearse el FIFO");
return(1);
}

//Abrimos el FIFO
if ((fd=open("FIFO_logger",O_RDONLY))<0){
perror("No puede abrirse el FIFO");
return(1);
}


while(1){
	if(read(fd, &punt, sizeof(punt))<sizeof(punt)){
	perror("error en lectura");
	break;
	}
	else{
		if(punt.JugadorGol==1){
			printf("El jugador 1 ha metido gol. Lleva %d\n", punt.PJugador1);

		}
		if(punt.JugadorGol==2){
                printf("El jugador 2 ha metido gol. Lleva %d\n", punt.PJugador2);
                }

	}
}

close(fd);
unlink("FIFO_logger");
return(0);
}

