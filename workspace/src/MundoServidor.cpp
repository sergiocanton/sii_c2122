// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////



#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <math.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char cad[200];

MundoServidor::MundoServidor()
{
	Init();
}

MundoServidor::~MundoServidor()
{
	close(fd); //cerramos la tuberia que conecta con el logger
	unlink("FIFO_logger");
	
	close(fdsc);
	unlink("/tmp/fifo_sc");
	
	close(fdt);
	unlink("/tmp/fifo_teclas");
	

}

void MundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void MundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void *hilo_comandos(void *d){
	MundoServidor *p = (MundoServidor*) d;
	p -> RecibeComandosJugador();
}

void MundoServidor::RecibeComandosJugador(){
	if ((fdt=open("/tmp/fifo_teclas",O_RDONLY ))<0){
	perror("No puede abrirse el FIFO para teclas");
	}
	while(1){
		usleep(10);
		char cad[100];
		unsigned char key;
		read(fdt, cad, sizeof(cad));
		sscanf(cad,"%c",&key);
		if(key == 's') jugador1.velocidad.y = -4;
		if(key == 'w') jugador1.velocidad.y = 4;
		if(key == 'l') jugador2.velocidad.y = -4;
		if(key == 'o') jugador2.velocidad.y = 4;
	}
}

void MundoServidor::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.puntos=puntos1+puntos2;


	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		if(esfera.puntos<5){
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		}else{
		esfera.radio=1;
		esfera.velocidad.x=5+2*rand()/(float)RAND_MAX;
                esfera.velocidad.y=5+2*rand()/(float)RAND_MAX;
		}
		punt.JugadorGol=2;
		puntos2++;
		punt.PJugador2=puntos2;
		write(fd, &punt, sizeof(punt));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		if(esfera.puntos<5){
                esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
                esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
                }else{
		esfera.radio=1;
                esfera.velocidad.x=-5-2*rand()/(float)RAND_MAX;
                esfera.velocidad.y=-5-2*rand()/(float)RAND_MAX;
                }
		punt.JugadorGol=1;
		puntos1++;
		punt.PJugador1=puntos1;
		write(fd, &punt, sizeof(punt));
	}
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	write(fdsc, &cad,strlen(cad));

}

void MundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void MundoServidor::Init()
{
//tuberia para la conexion entre cliente y servidor

//tuberia para logger
	if ((fd=open("FIFO_logger",O_WRONLY ))<0){
	perror("No puede abrirse el FIFO para el logger");
	}

	if ((fdsc=open("/tmp/fifo_sc",O_WRONLY ))<0){
	perror("No puede abrirse el FIFO servidor-cliente");
	}
	Plano p;
	
//thread

	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	
	pthread_create(&thid1, &atrib, hilo_comandos, this);
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


}
