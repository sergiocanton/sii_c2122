
# Changelog
En este archivo aparecerán los cambios realizados en el proyecto:

## [Practica3] - 2021-11-29
### v3.1
-Se ha creado logger.cpp que se encarga de indicar el marcador de tenis
-Se hac creado bot.cpp que se encarga de manejar la raqueta 1
## [Practica2] - 2021-11-02
### v2.1
- Se ha dado movimiento a la esfera
- Se ha dado movimiento a la raqueta
- El tamaño de la esfera y su velocidad cambian cuando el juego avanza

## [Practica1] - 2021-10-19
### v1.2
- Se ha finalizado la práctica.
### v1.1
- Se añadió mi nombre como autor en Esfera.h








